load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\movies (1).csv"
into table movies fields terminated by "," enclosed by '"';

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\countries (1).csv"
into table countries fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\stars (1).csv"
into table stars fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\movie_stars (1).csv"
into table movie_stars fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\directors (1).csv"
into table directors fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\movie_directors (1).csv"
into table movie_directors fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\genres (1).csv"
into table genres fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\languages (1).csv"
into table languages fields terminated by ",";

load data infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\producer_countries (1).csv"
into table producer_countries fields terminated by ",";

show variables like "secure_file_priv";
